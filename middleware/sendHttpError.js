'use strict';

module.exports = (req, res, next) => {
  res.sendHttpError = (err) => {
    res.status(err.status);
    if (res.req.headers['x-requested-with'] === 'XMLHttpRequest') { // если запрос был аяксом
      res.json(err);
    } else {
      res.render('error', {
        error: err,
        user: req.user
      });
    }
  };

  next();
};