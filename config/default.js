module.exports =
{
  server1: {
    port: 3000
  },
  server2: {
    port: 3001
  },
  session: {
    secret: 'kill_all_humans',
    name: 'nginx-sid',
    saveUninitialized: false,
    resave: false,
    cookie: {
      path: '/',
      httpOnly: true,
      maxAge: 7 * 24 * 60 * 60 * 1000
    }
  },
  redis: {
    host: 'localhost',
    port: 6379
  }
};