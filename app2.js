'use strict';

const
  express             = require('express'),
  app = express(),
  nunjucks            = require('nunjucks'),
  path                = require('path'),
  cfg                 = require('config'),
  favicon             = require('serve-favicon'),
  logger              = require('morgan'),
  cookieParser        = require('cookie-parser'),
  bodyParser          = require('body-parser'),
  session             = require('express-session'),
  routes              = require('./routes'),
  HttpError           = require('lib/HttpError');

nunjucks.configure('views', {// настройка шаблонизатора
  autoescape: true,
  express: app
});

app /* базовая настройка */
  .set('trust proxy', 1)
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'html')
  .set('view cache', true)
  .use(favicon(path.join(__dirname, 'public/img', 'favicon.ico')))
  .use(express.static(path.join(__dirname, 'public')))
  .use(logger('dev'))
  .use(require('middleware/sendHttpError'))
  .use(cookieParser())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: false}));

app /* роутинг, обработка ошибок */
  .use('/', routes.router2)
  .use(express.static(path.join(__dirname, 'storage')))
  .use((req, res, next) => { // обработка отсутствующей страницы
    next(new HttpError(404, 'Страница не найдена'));
  })
  .use((err, req, res, next) => {
    console.error(err);
  if (typeof err === 'number') {
    err = new HttpError(err);
  }
  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    if (app.get('env') === 'development') {
      res.sendHttpError(err);
    } else {
      err = new HttpError(500);
      res.sendHttpError(err);
    }
  }
  });


module.exports = app;