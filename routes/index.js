const
  router1 = require('express').Router(),
  router2 = require('express').Router(),
  pages  = require('./pages');

module.exports = {
  router1: router1,
  router2: router2
};


router1.get('/', pages.index);
router1.get('/page1', pages.sendPage1);
router1.post('/page1', pages.create);
router2.get('/page2', pages.sendPage2);