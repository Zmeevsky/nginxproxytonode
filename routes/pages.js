'use strict';
const
  redis = require("redis"),
  client = redis.createClient(),
  HttpError = require("lib/HttpError"),
  cfg = require("config");

module.exports = {
  index: index,
  sendPage1: sendPage1,
  sendPage2: sendPage2,
  create: createUser
};

function index(req, res, next) {
  res.redirect(302, '/page1');
}

function sendPage1(req, res, next) {
  res.render('page1', {user: req.user});
}

function sendPage2(req, res, next) {
  console.log(req.cookies);

  client.get(req.cookies[cfg.get('session.name')], (err, reply) => {
    if(err) return next(err);
    //console.log(reply);
    req.user = {name: reply ? reply : 'Аноним'};
    res.render('page2', {user: req.user});
  });
}

function createUser(req, res, next) {
  let
    data = req.body,
    sid = req.cookies[cfg.get('session.name')];

  if (!data || !sid) return next(new HttpError(400));

  client.set(sid, data.name, (err, reply) => {
    if(err) return next(err);

    res.redirect(302, 'http://localhost/page2');
  });
  
}